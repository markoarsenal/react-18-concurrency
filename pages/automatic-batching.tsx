import { useCallback, useEffect, useRef, useState } from "react";
import Button from "../components/Button";

const AutomaticBatching = () => {
  const [countOne, setCountOne] = useState(0);
  const [countTwo, setCountTwo] = useState(0);

  console.log("render");

  const onClick = useCallback(() => {
    setCountOne(countOne + 1);
    setCountTwo(countTwo + 1);
  }, [countOne, countTwo]);

  useEffect(() => {
    document.getElementById("native-event").addEventListener("click", onClick);
  }, [onClick]);

  const onClickAsync = () => {
    fetch("https://jsonplaceholder.typicode.com/todos/1").then(() => {
      setCountOne(countOne + 1);
      setCountTwo(countTwo + 1);
    });
  };

  const onClickTimeout = () =>
    setTimeout(() => {
      setCountOne(countOne + 1);
      setCountTwo(countTwo + 1);
    }, 200);

  return (
    <div className="p-4">
      <ul className="mb-8">
        <li>Count one: {countOne}</li>
        <li>Count two: {countTwo}</li>
      </ul>
      <Button onClick={onClick}>Batching in click event</Button>
      <Button id="native-event" className="ml-4">
        Batching in native click event
      </Button>
      <Button className="ml-4" onClick={onClickAsync}>
        Batching in fetch
      </Button>
      <Button className="ml-4" onClick={onClickTimeout}>
        Batching in timeout
      </Button>
    </div>
  );
};

export default AutomaticBatching;
