import NavLink from "../components/NavLink";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <ul className="flex justify-center gap-x-8 py-4">
        <li>
          <NavLink activeClassName="font-semibold" href="/">
            useTransition
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName="font-semibold" href="/use-deffered-value">
            useDefferedValue
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName="font-semibold" href="/automatic-batching">
            Automatic Batching
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName="font-semibold" href="/suspense">
            Suspense
          </NavLink>
        </li>
      </ul>
      <Component {...pageProps} />
    </div>
  );
}

export default MyApp;
