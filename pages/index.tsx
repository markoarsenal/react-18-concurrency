import { useState, useTransition } from "react";
import Button from "../components/Button";
import Comments from "../components/Comments";
import Modal from "../components/Modal";
import data from "../data/index.json";

export default function Home() {
  const [isOpen, setIsOpen] = useState(false);
  const [comments, setComments] = useState([]);
  const [pending, startTransition] = useTransition();

  return (
    <div className="p-4">
      <Button
        className="px-4 py-2 border-none rounded-sm bg-blue-800 text-white"
        onClick={() => {
          setIsOpen(true);
          startTransition(() => {
            setComments(data);
          });
        }}
      >
        Toggle modal
      </Button>
      <Modal
        isOpen={isOpen}
        onClose={() => {
          setIsOpen(false);
          setComments([]);
        }}
      >
        {pending ? "Loading..." : <Comments comments={comments} />}
      </Modal>
    </div>
  );
}
