import { useState, useDeferredValue } from "react";
import Comments from "../components/Comments";
import Modal from "../components/Modal";
import data from "../data/index.json";

export default function UseDeferredValues() {
  const [isOpen, setIsOpen] = useState(false);
  const [comments, setComments] = useState([]);
  const commentsToRender = useDeferredValue(comments);

  return (
    <div className="p-4">
      <button
        className="px-4 py-2 border-none rounded-sm bg-blue-800 text-white"
        onClick={() => {
          setIsOpen(true);
          setComments(data);
        }}
      >
        Toggle modal
      </button>
      <Modal
        isOpen={isOpen}
        onClose={() => {
          setIsOpen(false);
          setComments([]);
        }}
      >
        <Comments comments={commentsToRender} />
      </Modal>
    </div>
  );
}
