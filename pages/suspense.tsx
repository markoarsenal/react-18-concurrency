import { lazy, Suspense } from "react";

const LazyComponent = lazy(() => import("../components/LazyComponent"));

const SuspenseExample = () => {
  return (
    <div className="p-4">
      <Suspense fallback={<div>Loading...</div>}>
        <LazyComponent />
      </Suspense>
    </div>
  );
};

export default SuspenseExample;
