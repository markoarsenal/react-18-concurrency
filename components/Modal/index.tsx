import { PropsWithChildren } from "react";

type ModalProps = {
  isOpen: boolean;
  onClose: () => void;
};

const Modal = ({
  isOpen,
  children,
  onClose,
}: PropsWithChildren<ModalProps>) => {
  return isOpen ? (
    <div
      className="fixed inset-0 flex justify-center items-center bg-black bg-opacity-25"
      onClick={onClose}
    >
      <div
        className="w-8/12 h-96 p-4 overflow-y-auto rounded bg-white"
        onClick={(e) => e.stopPropagation()}
      >
        {children}
      </div>
    </div>
  ) : null;
};

export default Modal;
