import { memo, Profiler } from "react";
import Comment, { CommentProps } from "../Comment";

export type CommentsProps = {
  comments: CommentProps[];
  onClick?: () => void;
};

const Comments = ({ comments, onClick }: CommentsProps) => {
  return (
    <section>
      {comments.length
        ? comments.map((comment) => {
            return (
              <Profiler
                key={comment.id}
                id="comment"
                onRender={(id, phase, actualDuration, baseDuration) =>
                  console.log(id, phase, actualDuration, baseDuration)
                }
              >
                <Comment
                  {...comment}
                  className="mb-4"
                  onClick={onClick}
                  key={comment.id}
                />
              </Profiler>
            );
          })
        : "No data..."}
    </section>
  );
};

export default memo(Comments);
