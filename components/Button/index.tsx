import { PropsWithChildren, MouseEventHandler } from "react";
import clsx from "clsx";

type ButtonProps = {
  id?: string;
  className?: string;
  onClick?: MouseEventHandler<HTMLButtonElement>;
};

const Button = ({
  id,
  className,
  children,
  onClick,
}: PropsWithChildren<ButtonProps>) => {
  return (
    <button
      id={id}
      className={clsx(
        "px-4 py-2 border-none rounded-sm bg-blue-800 text-white",
        className
      )}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
